#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

set -euo pipefail

checkout()
{
    if [ -d osquery ]; then return; fi

    git clone https://github.com/osquery/osquery
    pushd osquery
    git checkout $version
    git submodule update --init -j $(nproc)
    popd
}

build()
{
    pushd osquery
    mkdir -p build
    pushd build

    # for now, AWS sdk can't be built
    # #error Atomics are not currently supported for non-x86 MSVC platforms
    # https://github.com/awslabs/aws-c-common/blob/4b4675c0670a0a55ddfd37cb9eda3712eba31fc4/include/aws/common/atomics_msvc.inl#L20
    cat > build.bat << EOF
call vcvarsall.bat /clean_env || exit 1
call "c:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Auxiliary/Build/vcvarsall.bat" x64_arm64 || exit 1
@echo on
::cmake -G "Visual Studio 16 2019" -A ARM64 -DOSQUERY_BUILD_TESTS=ON -DOSQUERY_BUILD_AWS=OFF .. || exit 1
::cmake --build . --config Release -j $(nproc) || exit 1
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DOSQUERY_BUILD_TESTS=ON -DOSQUERY_BUILD_AWS=OFF .. || exit 1
ninja || exit 1
::ninja test || exit 1
EOF

    cmd.exe /c build.bat

    popd
    popd
}

checkout
build
